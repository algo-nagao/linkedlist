//ポインタによる線形リストの実装
//Iterator による操作に対応

// リストセルクラス
class Cell<E>{
	E data;			// データ
	Cell <E> next;	// 次のデータへのリンク（ポインタ）

	Cell(E data, Cell<E> next){
		this.data = data;
		this.next = next;
	}
}
public class LinkedList<E> {
	// リストヘッダ
	public Cell<E> head;
	/* コンストラクタ
	 * 先頭ノードの扱いを統一するため、空のセルを使う
	 */
	LinkedList(){
		head = new Cell<E>(null,null);
	}

	/**
	 * リストの先頭に、指定された要素を追加します。
	 * @param e
	 */
	public void addFirst(E e){
		add(e,head);
	}
	/**
	 * リストの最後に、指定された要素を追加します。
	 * @param e
	 */
	public void addLast(E e){
		add(e,tail());
	}
	/**
	 * リストの指定された位置に、指定された要素を追加します。
	 * @param e 要素 node 親セル
	 */
	public void add(E e, Cell<E> cell){
		Cell p = new Cell(e,null);
		p.next = cell.next;
		cell.next = p;
	}

	/**
	 * リストの長さを返す
	 */
	public int size(){
		Cell <E> p = head;
		int n = 0;
		while (p.next != null){
			p = p.next;
			n++;
		}
		return n;
	}
	/**
	 * 指定されたインデックスのデータを削除し、要素を返す
	 * @param index
	 * @return 成功したか
	 */
	public boolean remove(Cell<E> target){
		Cell <E> prev = parent(target);
		if (prev==null)
			return false;
		prev.next = target.next;
		return true;
	}
	/**
	 * cellの親を返す
	 * @param cell
	 * @return
	 */
	public Cell<E> parent(Cell<E> cell){
		Cell<E> p = head;
		while(p.next!=null){
			if(p.next==cell)
				return p;
			p = p.next;
		}
		return null;
	}
	/**
	 * nodeの子を返す
	 * @param cell
	 * @return
	 */
	public Cell<E> child(Cell<E> cell){
		return cell.next;
	}
	/**
	 * リストに指定された要素がある場合にtrueを返す
	 * @param element
	 * @return 
	 */
	public Cell<E> search(E element){
		Cell<E> p = head.next;
		while(p!=null){
			if (p.data == element)
				return p;
			p = p.next;
		}
		return null;
	}
	/**
	 * リストの最後の要素を返す
	 * @param element
	 * @return 
	 */
	public Cell<E> tail(){
		Cell<E> p = head;
		while(p.next!=null){
			p = p.next;
		}
		return p;
	}
	/**
	 * リストの内容を文字列として返す
	 */
	public String toString(){
		String buf = "[";
		Cell<E> p = head;
		while(p.next!=null){
			p = p.next;
			buf += p.data.toString() + " ";
		}buf +="]";
		return buf;
	}
	/**
	 * リストのイテレータを得る
	 * @return イテレータ
	 */
	public LinkedListIterator<E> iterator(){
		return new LinkedListIterator<E>(this);
	}
}
