import java.util.*;

/**
 * 連結リスト(LinkedList)のイテレータ
 */
public class LinkedListIterator<E> implements ListIterator<E>{
	LinkedList mList;
	Cell<E> current; //	現在注目しているノード
	int index;
	// イテレータを生成
	LinkedListIterator(LinkedList<E> list){
		mList = list;
		current = list.head;
		index = 0;
	}
	// 要素を追加する
	public void add(E e){
		mList.add((E)e,current);
	}
	// 現在の要素にデータをセットする
	public void set(Object e){
		current.data = (E)e;
	}
	// 次の要素があるか
	public boolean hasNext(){
		return current.next != null;
	}
	// 次の要素を返す
	public E next(){
		if (current.next == null)
			throw new NoSuchElementException();
		current = current.next;
		index++;
		return current.data;
	}
	// 次の要素のインデックスを返す
	public int nextIndex(){
		return index+1;
	}
	// 前の要素があるか
	public boolean hasPrevious(){
		if (mList.parent(current)!=null)
			return true;
		else
			return false;
	}
	// 前の要素を返す
	public E previous(){
		if (index==0)
			throw new NoSuchElementException();
		current = mList.parent(current);
		index--;
		return current.data;
	}
	//前の要素のインデックスを返す
	public int previousIndex(){
		return index-1;
	}
	// 現在の要素を削除する
	public void remove(){
		mList.remove(current);
	}
}

